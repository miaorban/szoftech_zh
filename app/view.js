var View = (function()
{
	function Update(html){
		document.querySelector('main > div').innerHTML = html;
	}

	var self={
		Clear: function(){
			Update('');
		},

		EmptyState: function(interaction){
			Update('<section class="empty"><h2>Nincsenek adatok</h2><p>Add hozza az elso szemelyt!</p><i class="fas fa-plus-circle"></i></section>');
			document.querySelector('section.empty').onclick = interaction;
		},

		AddForm: function(submit){
			Update(`<form>
					<p id="success"><p>
					<label for="code">Code</label>
					<input type="number" id="code" required>

					<label for="sex">Sex</label>
					<select id="sex">
					  <option value="no">No</option>
					  <option value="ffi">Ferfi</option>
					  <option value="mercedes">Mercedes</option>
					  <option value="audi">Audi</option>
					</select>

					<label for="age">Eletkor</label>
					<input type="number" id="age"></input>

					<label for="rating">Rating</label>
					<input type="number" id="rating"></input>

					<button> <i class="fas fa-check"></i> Hozz�ad</button>

				</form>`);

				document.querySelector('form button').onclick = function(e){
					e.preventDefault();

					var data = {
						rating: document.querySelector('#rating').value,
						code: document.querySelector('#code').value,
						sex: document.querySelector('#sex').value,
						age: document.querySelector('#age').value
					};

					submit(data);
					document.querySelector("#list").click();
				};
		},

		List: function(model, interaction)
		{
			var html ='';
			let sumRatingMan = 0;
			let sumRatingWoman = 0;
			let sumAgeMan = 0;
			let sumAgeWoman = 0;
			let women = 0;
			let men = 0;
			for (var i in model){
				if (model[i].sex === 'no') {
					sumRatingWoman += parseInt(model[i].rating);
					sumAgeWoman += parseInt(model[i].age);
					women++;
				} else {
					sumRatingMan += parseInt(model[i].rating);
					sumAgeMan += parseInt(model[i].age);
					men++;
				}
			}
			sumRatingWoman = sumRatingWoman/women;
			sumRatingMan = sumRatingMan/men;
			sumAgeWoman = sumAgeWoman/women;
			sumAgeMan = sumAgeMan/men;

			html += ('<section><h2> Atlag ertekeles nok: '+sumRatingWoman+'</h2><p> Atlag kor nok:'+sumAgeWoman+'</p></section>');
			html += ('<section><h2> Atlag ertekeles ferfiak: '+sumRatingMan+'</h2><p> Atlag kor ferfiak:'+sumAgeMan+'</p></section>');
			html += ('<section><h2> Adatok szama: '+model.length+'</h2></section>');

			Update(html);

			var items = document.querySelectorAll('main section');
			for(var i=0; i<items.length;i++){
				items[i].onclick = TaskClick;
			}

			function TaskClick(){
				var x = parseInt(this.getAttribute('data-tx'));
				interaction(x);
			}

		}
	};

	return self;

})();
