var Model = (function(){
	var storage = null;

	function Init(){
		// singleton megnézi, hogy mi a storage értéke
		if(storage === null){
			storage = window.localStorage.getItem('model');

			//ha nincs ilyen elem
			if(!storage){
				storage = [];
				Sync();
			} else {
				//ha van => parse
				storage = JSON.parse(storage);
			}
		}
	}

	function Sync(){
		window.localStorage.setItem('model', JSON.stringify(storage));
	}

	Init();

	var self={
		AddNew: function({code, sex, age, rating}){
			Init();
			//tömbfeltöltés (végére)
			var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", 'http://ptex.hu/api/customercodevalidator.php?code='+code, false ); // false for synchronous request
	    xmlHttp.send( null );
			if (xmlHttp.responseText === '0') return;
			storage.push({
				code: code,
				sex: sex,
				age: age,
				rating: rating
			});
			Sync();
		},
		Remove: function(index){
			Init();
			if(index<storage.lenght)
			{
				storage.splice(index,1);
			}
			Sync();
		},
		GetAll: function(){
			Init();
			return storage;
		}
	};

	return self;

})();
