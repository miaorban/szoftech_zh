var Controller = (function(){
	function TaskList(){
		var  tasks = Model.GetAll();

		if(tasks.length > 0){
			View.List(tasks, function(x){Model.Remove(x); TaskList();});
		} else {
			View.EmptyState(AddNew);
		}
	};

	function AddNew(){
		View.AddForm(function(data){
			Model.AddNew(data);
		});
	};

	var self = {
		Run: function(){

			View.Clear();
			TaskList();

			document.querySelector('#add').onclick = function(e){
				e.preventDefault();
				AddNew();
			};
			document.querySelector("#list").onclick = function(e){
				e.preventDefault();
				TaskList();
			};
		}
	};

	return self;

})();
